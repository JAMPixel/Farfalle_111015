#include "Room.h"

#include "Location.h"
#include "Object.h"

namespace room {

	Object lightbulb{
		"ampoule",
		"Une ampoule de forme banale, mais qui semble être faite de cuivre ;\n"
			"Elle n'est retenue que par deux épais fils soudés. Le haut plafond vous\n"
			"oblige à vous tenir debout sur le lit ne serait-ce que pour l'effleurer."
	};

	Location bed{
		"lit",
		"Un lit, parfaitement bordé. Son apparence sobre contraste avec le reste des meubles.",
		{&lightbulb}
	};

	Location door{
		"porte",
		"Une porte en bois. Elle semble solide.",
		{&doorKnob}
	};

	Object doorKnob{
		"porte",
		door.lookAround
	};

	Location shelf{
		"étagère",
		"Une grande étagère ancienne, en bois massif.",
		{}
	};

	Location desk{
		"bureau",
		"Un imposant bureau de bois ouvragé à la surface en cuir noir. Au centre\n"
			"trône une machine à écrire ancienne, entourée de feuilles de papier,\n"
			"toutes vierges. Le bureau comporte deux tirois, dont un à serrure.",
		{}
	};

	Location chest{
		"malle",
		"Une grande malle verte, renforcée de laiton. Elle arbore une\n"
			"imposante serrure dorée, mais est entrouverte.",
		{}
	};

	std::vector<Location *> const topLocations{
		&bed, &door, &shelf, &desk, &chest
	};
}
