#include "Command.h"

#include <iostream>
#include <sstream>
#include <limits>
#include <algorithm>

#include "Context.h"
#include "Object.h"
#include "Input.h"
#include "Location.h"
#include "Room.h"

namespace commands {
	std::map<std::string, Command> const intrinsics{
		{"?", commands::help},
		{"quitter", commands::quit},
		{"aller", commands::go},
		{"regarder", commands::look},
		{"examiner", commands::examine},
		{"prendre", commands::pickUp},
		{"inventaire", commands::listInventory}
	};

	void help(Context &ctx, std::stringstream &) {
		for(auto const &kvp : intrinsics)
			std::cout << ". " << kvp.first << '\n';
	}

	void quit(Context &ctx, std::stringstream &lines) {
		lines.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Êtes-vous sûr ? (o/n) ";
		std::string const ans = getInputLine().str();

		if(ans == "o" || ans == "O")
			ctx.aboutToQuit = true;
	}

	void go(Context &ctx, std::stringstream &line) {
		std::string where = dump(line);
		if(where == "") {
			std::cout << "Vous pouvez aller vers :\n";
			for(Location const *loc : room::topLocations)
				std::cout << " - " << loc->name << '\n';
			return;
		}
		auto found = std::find_if(begin(room::topLocations), end(room::topLocations), [&where](Location *loc) {
			return loc->name == where;
		});
		if(found == end(room::topLocations)) {
			noCapito(where);
			return;
		}
		ctx.goTo(*found);
	}

	void look(Context &ctx, std::stringstream &) {
		std::cout << ctx.location().lookAround << '\n';
		if(!ctx.location().objects.empty()) {
			std::cout << "Vous voyez :\n";
			for(auto const &obj : ctx.location().objects)
				std::cout << " - " << obj->name << '\n';
		}
	}

	void examine(Context &ctx, std::stringstream &line) {
		std::string const what = dump(line);
		auto const &objs = ctx.location().objects;
		auto found = std::find_if(begin(objs), end(objs), [&what](auto const &obj) {
			return obj->name == what;
		});
		if(found != end(objs)) {
			std::cout << (*found)->lookAt << '\n';
			return;
		}

		auto const &objs2 = ctx.inventory;
		auto found2 = std::find_if(begin(objs2), end(objs2), [&what](auto const &obj) {
			return obj->name == what;
		});
		if(found2 != end(objs2)) {
			std::cout << (*found2)->lookAt << '\n';
			return;
		}

		std::cout << "Il n'y a pas de " << what << " ici.\n";
	}

	void pickUp(Context &ctx, std::stringstream &line) {
		std::string const what = dump(line);
		auto &objs = ctx.location().objects;
		auto found = std::find_if(begin(objs), end(objs), [&what](auto const &obj) {
			return obj->name == what;
		});
		if(found == end(objs)) {
			std::cout << "Il n'y a pas de " << what << " ici.\n";
			return;
		}

		ctx.inventory.push_back(*found);
		std::cout << "Vous prenez " << (*found)->name << ".\n";
		objs.erase(found);
	}

	void listInventory(Context &ctx, std::stringstream &line) {
		std::cout << "Vous avez :\n";
		for(Object *obj : ctx.inventory)
			std::cout << " - " << obj->name << '\n';
	}
}

namespace {
	void openDoor(Context &ctx) {
		std::cout << "Vous actionnez vigoureusement la poignée, mais la porte ne tremble même pas.\n";
	}
}

std::map<std::pair<std::string, Object *>, Action> const actions{
	{{"ouvrir", &room::doorKnob}, openDoor}
};
