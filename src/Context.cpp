#include "Location.h"

#include <iostream>
#include <sstream>
#include <algorithm>

#include "Object.h"
#include "Room.h"
#include "Context.h"
#include "Input.h"
#include "Command.h"

Context context;

Context::Context()
	: _currentLocation{&room::bed}
	, aboutToQuit{false} { }

void Context::goTo(Location *newLocation) {
	_currentLocation = newLocation;
	if(_currentLocation->firstSeen) {
		std::cout << _currentLocation->lookAround << '\n';
		_currentLocation->firstSeen = false;
	}
}

Location &Context::location() const {
	return *_currentLocation;
}

bool Context::act(std::string const &verb, std::stringstream &args) {
	std::string const object = dump(args);
	Object *obj = nullptr;

	auto found = std::find_if(begin(inventory), end(inventory), [&object](Object *obj) {
		return obj->name == object;
	});

	if(found != end(inventory)) {
		obj = *found;
	} else {
		auto &objects = location().objects;
		auto found = std::find_if(begin(objects), end(objects), [&object](Object *obj) {
			return obj->name == object;
		});

		if(found != end(objects))
			obj = *found;
	}

	if(!obj)
		return false;

	auto found2 = actions.find(std::make_pair(verb, obj));
	if(found2 != end(actions)) {
		found2->second(*this);
		return true;
	}

	return false;
}
