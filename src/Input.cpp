#include "Input.h"

#include <iostream>
#include <sstream>

std::stringstream getInputLine() {
	std::cout << "> ";
	std::string line;
	getline(std::cin, line);
	return std::stringstream(line);
}

std::stringstream &ignoreInput(std::stringstream &line, std::initializer_list<std::string> words) {
	for (; ;) {
		std::string word;
		auto linePos = line.tellg();
		if (!(line >> word))
			break;
		if (std::find(begin(words), end(words), word) == end(words)) {
			line.seekg(linePos);
			break;
		}
	}
	return line;
}

std::string nextWord(std::stringstream &line) {
	std::string word;
	line >> word;
	return word;
}

std::string dump(std::stringstream &line) {
	std::string str;
	line >> std::ws;
	getline(line, str);
	return str;
}

void noCapito(std::string const &what) {
	std::cout << "Je ne comprends pas \"" << what << "\".\n";
}
