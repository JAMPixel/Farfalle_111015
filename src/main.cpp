#include <iostream>
#include <map>
#include <sstream>

#include "Command.h"
#include "Context.h"
#include "Input.h"

int main() {

	std::cout << "Vous vous réveillez allongé(e) sur un lit. Vous êtes seul(e).\n"
		"En vous redressant, vous vous découvrez au centre d'une pièce inconnue.\n"
		"Les murs sont recouverts d'une tapisserie damassée, éclairés par une \n"
		"ampoule nue pendant du plafond au-dessus de vous.\n\n"
		"Malgré le lit qui en occupe le centre, cette pièce ressemble à une étude.\n"
		"Elle n'a qu'une seule porte, derrière vous, et pas de fenêtre.\n"
		"Face au pied du lit se dresse une grande étagère.\n"
		"À votre droite se trouve un bureau encombré de quelques fournitures.\n"
		"À votre gauche, le mur est orné de plusieurs tableaux, sous lesquels a\n"
		"été placée une grande malle.\n\n"
		"Vous reprenez vos esprits, constatez que vous êtes habillé(e) comme à\n"
		"l'accoutumée, cependant vos poches sont vides. Vous ne vous souvenez pas\n"
		"de la manière dont vous êtes arrivé(e) ici, ni même de la veille.\n\n";

	do {
		std::stringstream cmdStream{getInputLine()};

		std::string command;
		cmdStream >> command;

		auto found = commands::intrinsics.find(command);
		if(found == end(commands::intrinsics)) {
			if(!context.act(command, cmdStream))
				noCapito(command);
			continue;
		}

		(*found->second)(context, cmdStream);
	} while(!context.aboutToQuit);
}
