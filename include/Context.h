#pragma once

#include <vector>

struct Location;
struct Object;

struct Context {

	Context();

	Location &location() const;
	void goTo(Location *newLocation);

	bool act(std::string const &verb, std::stringstream &args);

private:
	Location *_currentLocation;
public:
	std::vector<Object *> inventory;
	bool aboutToQuit;
};

extern Context context;