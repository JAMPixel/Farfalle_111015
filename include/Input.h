#pragma once

#include <iosfwd>
#include <initializer_list>

std::stringstream getInputLine();

std::stringstream &ignoreInput(std::stringstream &line, std::initializer_list<std::string> words);

std::string nextWord(std::stringstream &line);

std::string dump(std::stringstream &line);

void noCapito(std::string const &what);
