#pragma once

#include <iosfwd>
#include <map>

struct Context;
struct Object;

using Command = void (*)(Context &ctx, std::stringstream &args);
using Action = void (*)(Context &ctx);

namespace commands {
	extern std::map<std::string, Command> const intrinsics;

	void help(Context &ctx, std::stringstream &args);

	void quit(Context &ctx, std::stringstream &args);

	void go(Context &ctx, std::stringstream &line);

	void look(Context &ctx, std::stringstream &args);

	void examine(Context &ctx, std::stringstream &args);

	void pickUp(Context &ctx, std::stringstream &line);

	void listInventory(Context &ctx, std::stringstream &line);
}

extern std::map<std::pair<std::string, Object *>, Action> const actions;
