#pragma once

#include <string>
#include <vector>
#include <memory>

struct Object;

struct Location {
	std::string name;
	std::string lookAround;
	std::vector<Object *> objects;
	bool firstSeen = true;
};
