#pragma once

#include <vector>

struct Location;
struct Object;

namespace room {
	extern std::vector<Location *> const topLocations;

	extern Location bed;
	extern Location door;
	extern Location shelf;
	extern Location desk;
	extern Location chest;

	extern Object lightbulb;
	extern Object doorKnob;
}
